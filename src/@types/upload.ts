export interface UploadedFile {
  id: string
  filename: string
  mimeType: string
  href: string
  size: number
}
