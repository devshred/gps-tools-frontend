# Frontend of GPS-Tools based on React and Vite
This is the frontend of [GPS-Tools Backend](https://github.com/devshred/gps-tools-backend)

# Production
[GPS-Tools](https://gps-tools.pages.dev/)

# How-to start locally
```sh
npm run dev
```

Open http://localhost:5173/
