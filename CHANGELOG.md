# [0.13.0](https://github.com/devshred/gps-tools-frontend/compare/v0.12.0...v0.13.0) (2024-05-23)


### Features

* **backend:** use new REST-API v1 ([35e35d5](https://github.com/devshred/gps-tools-frontend/commit/35e35d53af78f38be7ce8f19082c1a3f6dd759a9))

# [0.12.0](https://github.com/devshred/gps-tools-frontend/compare/v0.11.1...v0.12.0) (2024-05-06)


### Bug Fixes

* **i18n:** fix type issue at LanguageProvider ([92406b1](https://github.com/devshred/gps-tools-frontend/commit/92406b15265ac181f130e01d099d947b20113a86))


### Features

* **i18n:** language support of German and English ([9f34d66](https://github.com/devshred/gps-tools-frontend/commit/9f34d66bd23eed9546cb0d0908493eda446d220a))

## [0.11.1](https://github.com/devshred/gps-tools-frontend/compare/v0.11.0...v0.11.1) (2024-04-29)


### Bug Fixes

* **download:** fix editable trackname ([51148e0](https://github.com/devshred/gps-tools-frontend/commit/51148e0c9acfafac74c0635caa11f1d5bfe441d9))

# [0.11.0](https://github.com/devshred/gps-tools-frontend/compare/v0.10.0...v0.11.0) (2024-04-21)


### Features

* **merge:** fixed URL for merged file ([6161088](https://github.com/devshred/gps-tools-frontend/commit/616108851465b38e8427cc530c2c94c16ce80de1))
* **site:** describe project at about page ([825f5e3](https://github.com/devshred/gps-tools-frontend/commit/825f5e3f9315805ee2ce0a6165f06ee8c1a0a921))
* **visual:** add button to mute/disable polyline ([c01a9a2](https://github.com/devshred/gps-tools-frontend/commit/c01a9a2ed91aba2a7ba40f931892d60d1a4fcb9c))
* **visual:** integrate search for waypoints ([bda44bd](https://github.com/devshred/gps-tools-frontend/commit/bda44bdc1ae952c42c0058706393bc479646391b))

# [0.10.0](https://github.com/devshred/gps-tools-frontend/compare/v0.9.1...v0.10.0) (2024-04-12)


### Bug Fixes

* **layout:** fix build issue caused by map tilelayer-switcher ([44258dd](https://github.com/devshred/gps-tools-frontend/commit/44258dd3058efff7b08984bf9fb9c7d05636b657))
* **layout:** fix naming ([29f56cf](https://github.com/devshred/gps-tools-frontend/commit/29f56cf1444078e45577a8488052f136b020b798))
* **layout:** improve visibility ([ec59973](https://github.com/devshred/gps-tools-frontend/commit/ec59973bd8bbc1ad4829b3d4efd3bc47701bcd63))
* **react:** optimize re-rendering ([c3ebe9f](https://github.com/devshred/gps-tools-frontend/commit/c3ebe9f8de6254a6ab1def1e3f28e437baac8a6c)), closes [#4](https://github.com/devshred/gps-tools-frontend/issues/4)


### Features

* **download:** option to optimize waypoints ([2052422](https://github.com/devshred/gps-tools-frontend/commit/2052422cd6355060e2a3094d914a7ee8ea0525d4))
* **layout:** add intro text ([fbea6e5](https://github.com/devshred/gps-tools-frontend/commit/fbea6e5d37b499d2f9c6c9d9329fb4d32f00d5a0))
* **visual:** add additional tile providers ([b52b02e](https://github.com/devshred/gps-tools-frontend/commit/b52b02e50befec0ec594b47d85a0739b7a917a21))
* **visual:** add icons for waypoint types ([08b09e3](https://github.com/devshred/gps-tools-frontend/commit/08b09e3388e81bf7d1b8d4f9e37915928b016687))
* **visual:** change type of waypoints ([552bfa5](https://github.com/devshred/gps-tools-frontend/commit/552bfa517ae8b316841106d665cd2ea83887f7fd))
* **visual:** visualization is now based on GeoJSON ([4f30765](https://github.com/devshred/gps-tools-frontend/commit/4f307652a5d21585ff70b0d905e324c645f83b20))

## [0.9.1](https://github.com/devshred/gps-tools-frontend/compare/v0.9.0...v0.9.1) (2024-03-31)


### Bug Fixes

* **layout:** unclutter navbar ([649ca33](https://github.com/devshred/gps-tools-frontend/commit/649ca332dd26a0059c245f36ee4c9321b3d13b20))
* **merge:** improve error handling ([2f59b13](https://github.com/devshred/gps-tools-frontend/commit/2f59b13405c367c57a0cfb830eaac4c7cbdf893d))

# [0.9.0](https://github.com/devshred/gps-tools-frontend/compare/v0.8.0...v0.9.0) (2024-03-30)


### Bug Fixes

* **build:** fix build issues ([97b0b21](https://github.com/devshred/gps-tools-frontend/commit/97b0b21eabc97293d0679b2f0cf8b1cdcbc51864))
* **layout:** adjust wording to highlight new function to upload FIT-files ([87cda91](https://github.com/devshred/gps-tools-frontend/commit/87cda91a002e5f59e65d9e0f52ad29c2f97ab918))


### Features

* **layout:** better visibility with new styles, fonts and theme-switcher ([ba463a0](https://github.com/devshred/gps-tools-frontend/commit/ba463a07b5e46a30e02d810a12175b0e20a6a2f6))
* **visual:** add, change and remove waypoints ([180b4e4](https://github.com/devshred/gps-tools-frontend/commit/180b4e43b01de49420af607a1c8af3d638afeb07))

# [0.8.0](https://github.com/devshred/gps-tools-frontend/compare/v0.7.0...v0.8.0) (2024-03-22)


### Features

* **layout:** change theme-switcher to sun/moon ([d68820a](https://github.com/devshred/gps-tools-frontend/commit/d68820a77fce6688fbdc5d12b1e9613e29043fd8))
* **layout:** improve visibility of dropzone ([5482da2](https://github.com/devshred/gps-tools-frontend/commit/5482da2e82bbcce7b8ea83bcf158be13f43bb3e5))

# [0.7.0](https://github.com/devshred/gps-tools-frontend/compare/v0.6.0...v0.7.0) (2024-03-15)


### Features

* **download:** improve readability of download links ([8ac6444](https://github.com/devshred/gps-tools-frontend/commit/8ac64440cf9d208a224d066d6e682e5bb647df73))
* **layout:** improve UX ([c7f2b5b](https://github.com/devshred/gps-tools-frontend/commit/c7f2b5ba6aa97e342acd8f973b9464035cf6dfd9))

# [0.6.0](https://github.com/devshred/gps-tools-frontend/compare/v0.5.0...v0.6.0) (2024-03-12)


### Features

* **download:** download as TCX ([e50c4ed](https://github.com/devshred/gps-tools-frontend/commit/e50c4ed51035857bdce8b2fcd375f1b0c8e6747c))

# [0.5.0](https://github.com/devshred/gps-tools-frontend/compare/v0.4.0...v0.5.0) (2024-03-05)


### Bug Fixes

* **download:** sanitize edited trackname ([627e825](https://github.com/devshred/gps-tools-frontend/commit/627e82558c7b50f122854893c2b1db48e981404b))


### Features

* **download:** add functionality to change name of a track ([15a46c4](https://github.com/devshred/gps-tools-frontend/commit/15a46c40ed3d82c379cd3b8f351dfe9083366425))
* **visual:** show trackname ([5da413d](https://github.com/devshred/gps-tools-frontend/commit/5da413d35e23b7bbb97f47c9d3a41f18a4cd3bdb))

# [0.4.0](https://github.com/devshred/gps-tools-frontend/compare/v0.3.0...v0.4.0) (2024-02-29)


### Bug Fixes

* **layout:** label of merge-button ([54671ad](https://github.com/devshred/gps-tools-frontend/commit/54671adde64987ddeb438c9e7353767c4e689fc6))
* **merge:** avoid merging a single file ([7ae3eab](https://github.com/devshred/gps-tools-frontend/commit/7ae3eab398023e5259db4f3da9cb54c49c753422))


### Features

* **visual:** show waypoints ([cacf1f8](https://github.com/devshred/gps-tools-frontend/commit/cacf1f84a5288d3a6e92c8e2ebc048df075b39af))

# [0.3.0](https://github.com/devshred/gps-tools-frontend/compare/v0.2.0...v0.3.0) (2024-02-16)


### Features

* **layout:** adjust Alert ([4f7789b](https://github.com/devshred/gps-tools-frontend/commit/4f7789b41b412200d5adde55eb97c11fca260ffe))
* **upload:** add loading spinner ([9410406](https://github.com/devshred/gps-tools-frontend/commit/9410406ff594f4d94414da20776b1964d1f6b5e4))
* **upload:** handle backend responses (if any) ([bd92c2e](https://github.com/devshred/gps-tools-frontend/commit/bd92c2ed8addcdce598c18a3c8a3574afb551f05))

# [0.2.0](https://github.com/devshred/gps-tools-frontend/compare/v0.1.1...v0.2.0) (2024-02-15)


### Features

* increase timeout of feedbacks ([9c337f0](https://github.com/devshred/gps-tools-frontend/commit/9c337f05173e0f7c3d450a2205784015b25c94d3))
* use react-daisyui to style alerts ([17187b3](https://github.com/devshred/gps-tools-frontend/commit/17187b307e0a5203ad9663784cf40af816cf5926))
